<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *@ORM\Table(name="region")
 * @ORM\Entity()
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="regionNombre", type="string", length=200)
     */
    private $regionNombre;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set regionNombre
     *
     * @param string $regionNombre
     *
     * @return region
     */
    public function setRegionNombre($regionNombre)
    {
        $this->regionNombre = $regionNombre;

        return $this;
    }

    /**
     * Get regionNombre
     *
     * @return string
     */
    public function getRegionNombre()
    {
        return $this->regionNombre;
    }
    
    
}

