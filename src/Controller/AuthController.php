<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Region;

class AuthController extends AbstractController
{

    private $client;
    
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function register(Request $request): Response{
        
        $user['username'] = $request->get('email');
        $user['password'] = $request->get('password');
        $user['name_surname'] = $request->get('name_surname');
        $user['rut'] = $request->get('rut');
        $user['phone'] = $request->get('phone');
        if ($request->isMethod('post')) {
            $cartId = $request->getSession()->get('cartId');
            $apiToken = $this->getParameter('api_token');
            $apiBaseUrl = $this->getParameter('api_url');
            $user_json = json_encode($user);
            try {
                $response = $this->client->request(
                    'POST',
                    $apiBaseUrl.'register?access_token='.$apiToken,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'body' => $user_json,
                    ]
                );    
            } catch (Exception $e) {
                dump("f");
                // return $this->render('404.html.twig', $parameters);
            }
            
            $content = $response->toArray();
            if (!$content['created']) {
                
                $this->addFlash('error_register', $content['message']);
                
                //return $this->redirectToRoute('register',array('route'=> $current_route, 'request' => $request));
                //return $this->redirectToRoute('register', $request->query->all());
                return $this->render('register/register.html.twig',  $user);
            }else{
                if ($cartId) {
                    $request->query->set('route', 'carrito');    
                }
                $this->addFlash('successful_register', 'Tu cuenta se ha creado exitosamente');
                return $this->redirectToRoute('login', array('request' => $request,), 307);
            }
            
        }
        
        return $this->render('register/register.html.twig',  $user);
    }

    public function login(Request $request): Response
    {
        if ($request->isMethod('post')) {
            
            $user['username'] = $request->get('email');
            $user['password'] = $request->get('password');
            $route = $request->get('route');
            $apiToken = $this->getParameter('api_token');
            $apiBaseUrl = $this->getParameter('api_url');
            $user = json_encode($user);
            try {
                $response = $this->client->request(
                    'POST',
                    $apiBaseUrl.'login_check?access_token='.$apiToken,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'body' => $user,
                    ]
                );    
            } catch (Exception $e) {
                dump("f");
                // return $this->render('404.html.twig', $parameters);
            }

            if($response->getStatusCode() == 401){
                
                $request->getSession()->remove('token');
                $this->addFlash('error_login', 'Correo electrónico o contraseña incorrecta. Por favor, vuelve a intentarlo.');
                $current_route = $request->get('route');
                return $this->redirectToRoute('login',array('route'=> $current_route));
            }
            
            $content = $response->toArray();
            $request->getSession()->set('token',$content['token']);
            if($route)return $this->redirectToRoute($route);
            
            return $this->redirectToRoute('index');

        }
        
        
        return $this->render('login/login.html.twig');
    }

    public function logout(Request $request): Response
    {
        $request->getSession()->remove('token');
        return $this->redirectToRoute('index');
    }

    public function profile(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        $customer = [];
        $method = 'GET';
        if ($request->isMethod('POST')) {

            $customer["name"] = $request->get('name');
            $customer["rut"] = $request->get('rut');
            $customer["phone"] = $request->get('phone');
            $customer = json_encode($customer);
            $method = 'POST';
            $this->addFlash('success','¡Datos actualizados con éxito!');
        }
        try {
            $response = $this->client->request(
                $method,
                $apiBaseUrl.'auth/profile'.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                    'body' => $customer,
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
        if($response->getStatusCode() == 401){
            $request->getSession()->remove('token');
            $current_route = $request->get('_route');
            return $this->redirectToRoute('login',array('route'=> $current_route));
        }
        
        $regiones = $this->getDoctrine()
            ->getRepository(Region::class)
            ->findBy([],['regionNombre' => 'ASC']);


        $content = $response->toArray();
        $parameters["profile"] = $content;
        $parameters["regiones"] = $regiones;
        return $this->render('profile/profile.html.twig', $parameters);
    }

    public function wishlist(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'auth/favorites'.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
       
        $content = $response->toArray();
        $parameters["products"] = $content;
        $products = $parameters["products"];
        return $this->render('wishlist/wishlist.html.twig', $parameters);
    }

    public function addToWishlist(Request $request, $product_id): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        try {
            $response = $this->client->request(
                'POST',
                $apiBaseUrl.'auth/favorite/'.$product_id.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
        
        $content = $response->toArray();
        $data = array(
            "message" =>  "added",
        );
        $response = new JsonResponse($data);
        return $response;
    }

    public function removeFromWishlist(Request $request, $product_id): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        try {
            $response = $this->client->request(
                'DELETE',
                $apiBaseUrl.'auth/favorite/'.$product_id.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
       
        $content = $response->toArray();
        $this->addFlash('success', "deleted");
        return $this->redirectToRoute('wishlist');
    }

    public function getShippingInfo(Request $request, $id): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'auth/shipping/'.$id.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
    
        return new JsonResponse($response->toArray());
        $content = $response->toArray();
        $parameters["profile"] = $content;
        return $this->render('profile/profile.html.twig', $parameters);
    }

    public function setShippingInfo(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');

        $shipping_id = $request->get('id');
        //$shippingObject["name"] = $request->get('name');
        //$shippingObject["rut"] = $request->get('rut');
        $shippingObject["city"] = $request->get('city');
        $shippingObject["commune_id"] = $request->get('comuna_id');
        $shippingObject["region_id"] = $request->get('region_id');
        $shippingObject["address"] = $request->get('address');

        $shipping = json_encode($shippingObject);
        
        if($shipping_id){
            $method = "PUT";
            $url = $apiBaseUrl.'auth/shipping/'.$shipping_id.'?access_token='.$apiToken;
        }else{
            $method = "POST";
            $url = $apiBaseUrl.'auth/shipping?access_token='.$apiToken;
        }

        try {
            $response = $this->client->request(
                
                $method,
                $url,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                    'body' => $shipping,
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
        $content = $response->toArray();
        return $this->redirectToRoute('profile');
    }

    public function getBillingInfo(Request $request, $id): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'auth/billing/'.$id.'?access_token='.$apiToken,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
        
        return new JsonResponse($response->toArray());
        $content = $response->toArray();
        $parameters["profile"] = $content;
        return $this->render('profile/profile.html.twig', $parameters);
    }

    public function setBillingInfo(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');

        $shipping_id = $request->get('id');
      
        $shippingObject["name"] = $request->get('name');
        $shippingObject["rut"] = $request->get('rut');
        $shippingObject["city"] = $request->get('city');
        $shippingObject["commune_id"] = $request->get('comuna');
        $shippingObject["region_id"] = $request->get('region');
        $shippingObject["address"] = $request->get('address');

        $shipping = json_encode($shippingObject);
        
        if($shipping_id){
            $method = "PUT";
            $url = $apiBaseUrl.'auth/billing/'.$shipping_id.'?access_token='.$apiToken;
        }else{
            $method = "POST";
            $url = $apiBaseUrl.'auth/billing?access_token='.$apiToken;
        }

        try {
            $response = $this->client->request(
                
                $method,
                $url,
                [
                    'headers' => [
                        'Authorization' => 'Bearer {'.$token.'}',
                    ],
                    'body' => $shipping,
                ]
            );
        } catch (Exception $e) {
            dump("f", $e);
            // return $this->render('404.html.twig', $parameters);
        }
        $content = $response->toArray();
        return $this->redirectToRoute('profile');
    }

    public function recoverPassword(Request $request): Response
    {
        if ($request->isMethod('post')) {
            
            $user['username'] = $request->get('email');
            $route = $request->get('route');
            $apiToken = $this->getParameter('api_token');
            $apiBaseUrl = $this->getParameter('api_url');
            $user = json_encode($user);
            try {
                $response = $this->client->request(
                    'POST',
                    $apiBaseUrl.'recover-password?access_token='.$apiToken,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'body' => $user,
                    ]
                );    
            } catch (Exception $e) {
                dump("f");
            }

            if($response->getStatusCode() == 400){
                
                $content = $response->toArray(false);        
                $msg = $content['message'];
                $this->addFlash('error_recover',$msg);
                return $this->redirectToRoute('recoverPassword');
            }

            $content = $response->toArray();        
            $msg = $content['message'];
            return $this->render('recover/checkEmail.html.twig', array('msg'=>$msg));

        }
        
        return $this->render('recover/recoverPassword.html.twig');
    }

    public function resettingPassword(Request $request, $token): Response
    {
        if ($request->isMethod('post')) {
            
            $user['token'] = $token;
            $user['plainPassword']['first'] = $request->get('first');
            $user['plainPassword']['second'] = $request->get('second');

            $route = $request->get('route');
            $apiToken = $this->getParameter('api_token');
            $apiBaseUrl = $this->getParameter('api_url');
            $user = json_encode($user);
            try {
                $response = $this->client->request(
                    'POST',
                    $apiBaseUrl.'resetting-password?access_token='.$apiToken,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                        ],
                        'body' => $user,
                    ]
                );    
            } catch (Exception $e) {
                dump("f");
            }

            if($response->getStatusCode() == 400){
                
                $content = $response->toArray(false);        
                $msg = $content['message'];
                $this->addFlash('error_resetting',$msg);

                return $this->redirectToRoute('resettingPassword',array('token'=> $token));
            }
            
            $this->addFlash('aviso_recover','Contraseña cambiada con éxito');
            return $this->redirectToRoute('login');

        }
        
        
        return $this->render('recover/resettingPassword.html.twig');
    }

}