<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Comuna;
use App\Entity\Region;

class DefaultController extends AbstractController
{
    
    private $client;
    
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function index(): Response
    {
        return $this->render('index.html.twig');
    }
    
    // public function contacto(): Response
    // {
    //     return $this->render('contacto.html.twig');
    // }
    
    public function pyme(Request $request, $comuna, $slug): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'datos-empresa/'.$slug.'?access_token='.$apiToken
            );
        } catch (Exception $e) {
            dump("f", $e);
            //return $this->render('404.html.twig', $parameters);
        }
        try {
            $response2 = $this->client->request(
                'GET',
                $apiBaseUrl.'marketplace/'.$slug.'/products?access_token='.$apiToken
            );
        } catch (Exception $e) {
            dump("f", $e);
            //return $this->render('404.html.twig', $parameters);
        }
        $content = $response->toArray();
        $content2 = $response2->toArray();
        $parameters['empresa'] = $content;
        $parameters['productos'] = $content2;
        
        return $this->render('pyme.html.twig', $parameters);
    }

    public function tienda(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $search = $request->get('search');
        $orderby = $request->get('orderby');
        $pages = $request->get('pages');
        $pagesize = $request->get('pagesize');
        $category = $request->get('category');
        $subcategory = $request->get('subcategory');

    
        if (!$pagesize)  {
            $pagesize = 9;
        }
        if (!$orderby)  {
            $orderby = 'alphabetical-asc';
        }
        if (!$pages) {
            $pages = 1;
        }      

     
        if($search == null or $search == ""){
           
            try {             
                $response = $this->client->request(
                    'GET',
                    //$apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&orderby='.$orderby
                    $apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&pages='.$pages.'&orderby='.$orderby.'&pagesize='.$pagesize.'&category='.$category.'&subcategory='.$subcategory
                );
            } catch (Exception $e) {
                dump("f", $e);
                // return $this->render('404.html.twig', $parameters);
            }
        }else{
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&name='.$search.'&orderby'.$orderby.'&pages='.$pages.'&pagesize='.$pagesize
                );
            } catch (Exception $e) {
                dump("f", $e);
                //return $this->render('404.html.twig', $parameters);
            }
        }
        $content = $response->toArray();
        $parameters["products"] = $content;
        $parameters["search"] = $search;
        $parameters["pagesize"] = $pagesize;
        $parameters["orderby"] = $orderby;
        $parameters["pages"] = $pages;
        $products = $parameters["products"];
       
        return $this->render('tienda.html.twig', $parameters);
    }
    
    public function tiendaPassProductora(Request $request,$pais , $productora): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $search = $request->get('search');
        $orderby = $request->get('orderby');
        $pages = $request->get('pages');
        $pagesize = $request->get('pagesize');
        $category = $request->get('category');
        $subcategory = $request->get('subcategory');


        if (!$pagesize) {
            $pagesize = 9;
        }
        if (!$orderby) {
            $orderby = 'alphabetical-asc';
        }
        if (!$pages) {
            $pages = 1;
        }


        if ($search == null or $search == "") {

            try {
                $response = $this->client->request(
                    'GET',
                    //$apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&orderby='.$orderby
                    $apiBaseUrl . 'pass-productora/'. $productora.'?access_token=' . $apiToken . '&pages=' . $pages . '&orderby=' . $orderby . '&pagesize=' . $pagesize . '&category=' . $category . '&subcategory=' . $subcategory
                );
            } catch (Exception $e) {
                dump("f", $e);
                // return $this->render('404.html.twig', $parameters);
            }
        } else {
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl . 'pass-productora/'. $productora.'?access_token=' . $apiToken . '&name=' . $search . '&orderby' . $orderby . '&pages=' . $pages . '&pagesize=' . $pagesize
                );
            } catch (Exception $e) {
                dump("f", $e);
                //return $this->render('404.html.twig', $parameters);
            }
        }
        $content = $response->toArray();
        $parameters["pass"] = $content["pass"];
        $parameters["banner"] = $content["banner"];
        $parameters["urlBanner"] = $content["urlBanner"];
        $parameters["productora"] = $productora;
        $parameters["search"] = $search;
        $parameters["pagesize"] = $pagesize;
        $parameters["orderby"] = $orderby;
        $parameters["pages"] = $pages;
        $pass = $parameters["pass"];

        return $this->render('tiendaPass.html.twig', $parameters);
    }

    public function tiendaPassSlug(Request $request,$pais, $slugEventoPass): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $search = $request->get('search');
        $orderby = $request->get('orderby');
        $pages = $request->get('pages');
        $pagesize = $request->get('pagesize');
        $category = $request->get('category');
        $subcategory = $request->get('subcategory');


        if (!$pagesize) {
            $pagesize = 9;
        }
        if (!$orderby) {
            $orderby = 'alphabetical-asc';
        }
        if (!$pages) {
            $pages = 1;
        }


        if ($search == null or $search == "") {

            try {
                $response = $this->client->request(
                    'GET',
                    //$apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&orderby='.$orderby
                    $apiBaseUrl . 'pass-evento/'.$slugEventoPass.'?access_token=' . $apiToken . '&pages=' . $pages . '&orderby=' . $orderby . '&pagesize=' . $pagesize . '&category=' . $category . '&subcategory=' . $subcategory
                );
            } catch (Exception $e) {
                dump("f", $e);
                // return $this->render('404.html.twig', $parameters);
            }
        } else {
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl . 'pass-evento/'.$slugEventoPass.'?access_token=' . $apiToken . '&name=' . $search . '&orderby' . $orderby . '&pages=' . $pages . '&pagesize=' . $pagesize
                );
            } catch (Exception $e) {
                dump("f", $e);
                //return $this->render('404.html.twig', $parameters);
            }
        }
        $content = $response->toArray();
        $parameters["pass"] = $content["pass"];
        $parameters["urlBanner"] = $content["urlBanner"];
        $parameters["banner"] = $content["banner"];
        $parameters["productora"] = $slugEventoPass;
        $parameters["pais"] = $pais;
        $parameters["search"] = $search;
        $parameters["pagesize"] = $pagesize;
        $parameters["orderby"] = $orderby;
        $parameters["pages"] = $pages;
        $pass = $parameters["pass"];

        return $this->render('tiendaPass.html.twig', $parameters);
    }
    public function tiendaPassProductoraEventos(Request $request,$pais, $slugEventoPass): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $search = $request->get('search');
        $orderby = $request->get('orderby');
        $pages = $request->get('pages');
        $pagesize = $request->get('pagesize');
        $category = $request->get('category');
        $subcategory = $request->get('subcategory');


        if (!$pagesize) {
            $pagesize = 9;
        }
        if (!$orderby) {
            $orderby = 'alphabetical-asc';
        }
        if (!$pages) {
            $pages = 1;
        }


        if ($search == null or $search == "") {

            try {
                $response = $this->client->request(
                    'GET',
                    //$apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&orderby='.$orderby
                    $apiBaseUrl . 'pass-productora/'.$slugEventoPass.'/eventos?access_token=' . $apiToken . '&pages=' . $pages . '&orderby=' . $orderby . '&pagesize=' . $pagesize . '&category=' . $category . '&subcategory=' . $subcategory
                );
            } catch (Exception $e) {
                dump("f", $e);
                // return $this->render('404.html.twig', $parameters);
            }
        } else {
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl . 'pass-productora/'.$slugEventoPass.'/eventos?access_token=' . $apiToken . '&name=' . $search . '&orderby' . $orderby . '&pages=' . $pages . '&pagesize=' . $pagesize
                );
            } catch (Exception $e) {
                dump("f", $e);
                //return $this->render('404.html.twig', $parameters);
            }
        }
        $content = $response->toArray();
        $parameters["pass"] = $content["pass"];
        $parameters["banner"] = $content["banner"];
        $parameters["urlBanner"] = $content["urlBanner"];
        $parameters["productora"] = $slugEventoPass;
        $parameters["pais"] = $pais;        
        $parameters["search"] = $search;
        $parameters["pagesize"] = $pagesize;
        $parameters["orderby"] = $orderby;
        $parameters["pages"] = $pages;
        $pass = $parameters["pass"];
        return $this->render('tiendaProductoraEventos.html.twig', $parameters);
    }

    public function tiendaPass(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $search = $request->get('search');
        $orderby = $request->get('orderby');
        $pages = $request->get('pages');
        $pagesize = $request->get('pagesize');
        $category = $request->get('category');
        $subcategory = $request->get('subcategory');


        if (!$pagesize) {
            $pagesize = 9;
        }
        if (!$orderby) {
            $orderby = 'alphabetical-asc';
        }
        if (!$pages) {
            $pages = 1;
        }


        if ($search == null or $search == "") {

            try {
                $response = $this->client->request(
                    'GET',
                    //$apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&orderby='.$orderby
                    $apiBaseUrl . 'lista-pass?access_token=' . $apiToken . '&pages=' . $pages . '&orderby=' . $orderby . '&pagesize=' . $pagesize . '&category=' . $category . '&subcategory=' . $subcategory
                );
            } catch (Exception $e) {
                dump("f", $e);
                // return $this->render('404.html.twig', $parameters);
            }
        } else {
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl . 'lista-pass?access_token=' . $apiToken . '&name=' . $search . '&orderby' . $orderby . '&pages=' . $pages . '&pagesize=' . $pagesize
                );
            } catch (Exception $e) {
                dump("f", $e);
                //return $this->render('404.html.twig', $parameters);
            }
        }
        $content = $response->toArray();
        $parameters["pass"] = $content;
        $parameters["search"] = $search;
        $parameters["pagesize"] = $pagesize;
        $parameters["orderby"] = $orderby;
        $parameters["pages"] = $pages;
        $pass = $parameters["pass"];

        return $this->render('tiendaPass.html.twig', $parameters);
    }
    
    public function producto($slug): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'products/slug/'.$slug.'?access_token='.$apiToken
            );
            $response2 = $this->client->request(
                'GET',
                $apiBaseUrl.'marketplace/products?access_token='.$apiToken.'&pages=1&pagesize=4'
            );
        } catch (Exception $e) {
            dump("f");
            // return $this->render('404.html.twig', $parameters);
        }
        $content = $response->toArray();
        $content2 = $response2->toArray();
        $tags = explode(",", $content['tags']);
        $parameters["product"] = $content;
        $parameters["products"] = $content2;
        
       
        return $this->render('producto.html.twig', $parameters);
    }

    public function carrito(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $cartId = $request->getSession()->get('cartId');
        
        if ($cartId != null){
            try {
                $response = $this->client->request(
                    'GET',
                    $apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken
                );
            } catch (Exception $e) {
                dump("f");
                // return $this->render('404.html.twig', $parameters);
            }
            $cart = $response->toArray();
            /*if(count($cart) == 1){
                $cart = $cart[0];
            }*/
            $parameters["cart"] = $cart;
            $parameters["subtotal"] = $cart['subtotal'];
            $parameters["code"] = $request->get('code');
        }else{
            $parameters["cart"] = null;
            $parameters["code"] = null;
        }
        
        return $this->render('carrito.html.twig', $parameters);
    }

    
    public function addToCart(Request $request)
    {
        $productObject["product_id"] = $request->get('product_id');
        $productObject["variant_id"] = $request->get('variant_id');
        $productObject["quantity_products"] = $request->get('quantity_products');
        
        
        if($request->getSession()->get('cartId') == null){
            
            $productObject["cart_id"] = null;
        }
        else {
            $productObject["cart_id"] = $request->getSession()->get('cartId');
        }
        //$formData = new FormDataPart($productObject);
        $product = json_encode($productObject);
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $response = $this->client->request(
            'POST',
            $apiBaseUrl.'products/shopping-cart?access_token='.$apiToken,
            [
                'body' => $product,
            ]
        );
            
        $content = $response->toArray();
        $request->getSession()->set('cartId',$content['cart_id']);
        //return $this->redirectToRoute('carrito');
        return new JsonResponse($content['cart_id']);
    }
    
    
    public function addToCartPass(Request $request)
    {
        $passObject["pass_id"] = $request->get('pass_id');
        $passObject["quantity_pass"] = $request->get('quantity_pass');
        $passObject["adultos"] = $request->get('adultos');
        $passObject["ninios"] = $request->get('ninios');
        
        if($request->getSession()->get('cartId') == null){
            
            $passObject["cart_id"] = null;
        }
        else {
            $passObject["cart_id"] = $request->getSession()->get('cartId');
        }
        //$formData = new FormDataPart($passObject);
        $pass = json_encode($passObject);
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $response = $this->client->request(
            'POST',
            $apiBaseUrl.'pass/shopping-cart?access_token='.$apiToken,
            [
                'body' => $pass,
            ]
        );
        
        $content = $response->toArray();
        $request->getSession()->set('cartId',$content['cart_id']);
        //return $this->redirectToRoute('carrito');
        return new JsonResponse($content['cart_id']);
    }

    public function currentItems(Request $request): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $cartId = $request->getSession()->get('cartId');
        
        if ($cartId != null){
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken
            );

            $cart = $response->toArray();

            /*if(count($cart) == 1){
                $cart = $cart[0];
            }*/
                
            return new JsonResponse($cart);
        }else{
            return new JsonResponse(null);
        }
    }

    public function deleteFromCartPass(Request $request, $saleId): Response
    {   
        $code = $request->get('code');
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');

        // Solicitud DELETE para eliminar el producto del carrito
        try {
            $response = $this->client->request(
                'DELETE',
                $apiBaseUrl . 'pass/shopping-cart/sale-pass/' . intval($saleId) . '?access_token=' . $apiToken
            );
        } catch (\Exception $e) {
            $this->addFlash('error', 'Error al eliminar el producto del carrito.');
            return $this->redirectToRoute('carrito', array('code' => $code));
        }
        if (empty($cart['products_sales']) && empty($cart['pass_sales'])) {
            $request->getSession()->remove('cartId');
        }
        // Redirigir al carrito después de la eliminación
        return $this->redirectToRoute('carrito', array('code' => $code));
    }

    public function deleteFromCart(Request $request,$saleId): Response
    {
        $code = $request->get('code');
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $response = $this->client->request(
            'DELETE',
            $apiBaseUrl.'products/shopping-cart/sale/'.strval($saleId).'?access_token='.$apiToken
        );
        
        $cartId = $request->getSession()->get('cartId');
        if ($cartId != null)
        {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken
            );
    
        $cart = $response->toArray(); 
       
         if($cart['products_sales']==[]){
            $request->getSession()->remove('cartId');
         }
        }
        return $this->redirectToRoute('carrito', array('code'=>$code));
    }

    public function checkout(Request $request): Response
    {

        /*if (is_null($request->headers->get('referer'))) {
            return $this->redirectToRoute('shippingInformation');
        }*/
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        $current_route = $request->get('_route');
        if(!$token){
            return $this->redirectToRoute('login',array('route'=> $current_route));
        }

        //perfil de usuario
        $profileResponse = $this->client->request(
            'GET',
            $apiBaseUrl.'auth/profile'.'?access_token='.$apiToken,
            [
                'headers' => [
                    'Authorization' => 'Bearer {'.$token.'}',
                ],
            ]
        );

        if($profileResponse->getStatusCode() == 401){
            $request->getSession()->remove('token');
            return $this->redirectToRoute('login',array('route'=> $current_route));
        }
        $profile = $profileResponse->toArray();
        
        // redireccion si carro no existe (osea no tiene productos)
        $cartId = $request->getSession()->get('cartId');
        if($cartId == null){
            return $this->redirectToRoute('index');
        }
        
        if ($request->isMethod('POST')) {
            $methods_shipping = $request->get('shipping_method');
            $data_shipping = array();
            foreach ($methods_shipping as $method) {
                $data = explode('-', $method);
                $shipping = array(
                    'shippingCart' => $data[0],
                    'shippingMethod' => $data[1]
                );
                $data_shipping[]=$shipping;
            }

            $customerObject["formaPago"] = 7;
            $customerObject["currency"] = "CLP";
            $customerObject["method_shipping_id"] = intval(1); 
            $customerObject["data_shipping"] = $data_shipping;

            $customer = json_encode($customerObject);
            $response = $this->client->request(
                'POST',
                $apiBaseUrl.'products/shopping-cart/checkout/'.$cartId.'?access_token='.$apiToken,
                [
                    'body' => $customer,
                ]
            );
            $response = $response->toArray();
            /*var_dump($response);
            return true;*/
            //$response = $response;
            
            $cartId = $response['cart_id'];
            $cartCode = $response['cart_code'];
            $price = $response['converted_total_price'];
            $pagoBase64 = base64_encode($cartId."@;".$cartCode."@;".$price);
            
            $urlBase = $response['payment_url'];
            $webpayURL = $urlBase.'?data='.$pagoBase64;
            /*var_dump($webpayURL);
            return true;
            */return $this->redirect($webpayURL);
        }
        $response = $this->client->request(
            'GET',
            $apiBaseUrl.'products/shopping-cart/checkout/'.$cartId.'?access_token='.$apiToken
        );
    
        $cart = $response->toArray();
        $parameters["cart"] = $cart['shopping_cart'];
        $parameters["shipping_info"] = $cart['shipping_info'];
        $parameters["deleted_products"] = $cart['deleted_products'];
        $parameters["profile"] = $profile;
        //$parameters["shipping_address"] = $cart['shipping_address'];
        
        return $this->render('checkout.html.twig', $parameters);
    }

    public function changeQuantityShoppingCart(Request $request)
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $locale = $request->getLocale();

        $variant_Id = $request->query->get('variantId');
        $product_Id = $request->query->get('productId');
        $quantity_products = $request->query->get('quantity_products');
        $cart_Id = $request->query->get('cartId');
        $discounted_price = $request->query->get('discountedPrice');
        if($discounted_price == 0){
            $discounted_price = null;
        }
        $response = $this->client->request(
            'GET',
            $apiBaseUrl.'products/shopping-cart/change/quantity?variant_Id='.$variant_Id.'&product_Id='.$product_Id.'&quantity_products='.$quantity_products.'&cart_Id='.$cart_Id.'&discounted_price='.$discounted_price.'&access_token='.$apiToken
        );
          return new JsonResponse($response->getContent());
    }

    public function getComunas(Request $request, $regionId){

        $comunas = $this->getDoctrine()
            ->getRepository(Comuna::class)->getComunas($regionId);
        return new JsonResponse($comunas);
    }

    public function shippingInformation(Request $request): Response
    {

        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');
        $token = $request->getSession()->get('token');
        $current_route = $request->get('_route');
        
        if(!$token){
            return $this->redirectToRoute('login',array('route'=> $current_route));
        }

        //perfil de usuario
        $profileResponse = $this->client->request(
            'GET',
            $apiBaseUrl.'auth/profile'.'?access_token='.$apiToken,
            [
                'headers' => [
                    'Authorization' => 'Bearer {'.$token.'}',
                ],
            ]
        );

        if($profileResponse->getStatusCode() == 401){
            $request->getSession()->remove('token');
            return $this->redirectToRoute('login',array('route'=> $current_route));
        }
        $profile = $profileResponse->toArray();
        
        $shippingMethods = null;
        // redireccion si carro no existe (osea no tiene productos)
        $cartId = $request->getSession()->get('cartId');
        if($cartId == null){
            return $this->redirectToRoute('index');
        }
        
        if ($request->isMethod('POST')) {
            $alternative = $request->get('alternative_shipping');
            
            $customerObject["shippingName"] = $request->get('shippingName');
            $customerObject["shippingRut"] = $request->get('shippingRut');
            $customerObject["shippingCity"] = $request->get('shippingCity');
            $customerObject["shippingCommune"] = $request->get('comuna_id');
            $customerObject["shippingRegion"] = $request->get('region_id');
            $customerObject["shippingAddress"] = $request->get('shippingAddress');
            $customerObject["billingName"] = $request->get('shippingName');
            $customerObject["billingRut"] =  $request->get('costumerRut');
            $customerObject["billingCity"] = $request->get('shippingCity');
            $customerObject["billingCommune"] =  $request->get('comuna_id');
            $customerObject["billingAddress"] = $request->get('shippingAddress');
            
            $customerObject["costumerName"] = $request->get('costumerName');
            $customerObject["costumerPhone"] = $request->get('costumerPhone');
            $customerObject["costumerEmail"] = $request->get('costumerEmail');
            $customerObject["costumerRut"] = $request->get('costumerRut');

            $customerObject['user_id'] = $profile['user_id'];
            $customer = json_encode($customerObject);

            $response = $this->client->request(
                'POST',
                $apiBaseUrl.'products/shopping-cart/shipping/'.$cartId.'?access_token='.$apiToken,
                [
                    'body' => $customer,
                ]
            );
            
            return $this->redirectToRoute('checkout');
        }
        $response = $this->client->request(
            'GET',
            $apiBaseUrl.'products/shopping-cart/'.$cartId.'?access_token='.$apiToken
        );
    
        $cart = $response->toArray();
        $regiones = $this->getDoctrine()
            ->getRepository(Region::class)
            ->findBy([],['regionNombre' => 'ASC']);
        $parameters["regiones"] = $regiones;
        $parameters["cart"] = $cart;
        $parameters["shippingMethods"] = $shippingMethods;
        $parameters["profile"] = $profile;
        
        return $this->render('shippingInformation.html.twig', $parameters);
    }

    public function pagado(Request $request, $id): Response
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');

        $response = $this->client->request(
            'GET',
            $apiBaseUrl.'products/shopping-cart/'.$id.'?access_token='.$apiToken
        );
    
        $cart = $response->toArray();
        
        $parameters["cart"] = $cart;
        $request->getSession()->remove('cartId');
        return $this->render('pagado.html.twig', $parameters);
    }

    public function rechazo(Request $request, $id): Response
    {
        $request->getSession()->remove('cartId');
        return $this->render('rechazo.html.twig');
    }

    public function error(Request $request, $id): Response
    {
        $request->getSession()->remove('cartId');
        return $this->render('error.html.twig');
    }

    public function checkProductVariant(Request $request)
    {
        $apiToken = $this->getParameter('api_token');
        $apiBaseUrl = $this->getParameter('api_url');

        $variantName = $request->query->get('variantName');
        $productId = $request->query->get('productId');
        
        try {
            $response = $this->client->request(
                'GET',
                $apiBaseUrl.'products/check/variant?variantName='.$variantName.'&productId='.$productId.'&access_token='.$apiToken
            );

            $content = $response->toArray();
            
            $parameters["product"] = $content;
        } catch (Exception $e) {
            dump("f");
        }

          return new JsonResponse($content);
    }

}
    