<?php

namespace App\Repository;

use App\Entity\Comuna;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class ComunaRepository extends ServiceEntityRepository
{   
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comuna::class);
    }

	public function getComunas($regionId){

        $qb = $this->createQueryBuilder('p')
            ->select('p.id, p.nombre')
            ->innerjoin('App\Entity\Provincia', 'b', 'WITH', 'b.id = p.provincia')
            ->innerjoin('App\Entity\Region', 'c', 'WITH', 'c.id = b.region')
            //->where('c.id = :regionId')->setParameter('regionId', $regionId)
            //->where('p.provincia = :provincia')
            ->where('c.id = :regionId')->setParameter('regionId', $regionId)
            //->setParameter('provincia', 14)
            ->orderBy('p.nombre', 'ASC');

        $query = $qb->getQuery();

        return $query->execute();
    }
    
}
