/*regiones.forEach(elem => {
	console.log('elemento: '+ elem.id);
	$("#regiones").append($("<option>", {
	    value: elem.id,
	    text: elem.nombre
	 }));
});*/

//consultar por comunas de acuerdo a la region
function comunas(){
    
    var region_id = $('#region_id').val();
    var parametros = {
        "regionId":region_id
    };
    $("#comuna_id").find('option').remove();
    $.ajax({
        url: Routing.generate("getComunas",parametros),
        type: 'GET',
        //data: parametros,
        dataType: "json",
        success: function (response) {
            $("#comuna_id").append('<option value="">Seleccionar</option>');
            $(response).each(function(){
                $("#comuna_id").append('<option value="' + this.id + '">' + this.nombre + '</option>');
                //console.log('comuna: '+this.nombre);
            });
        }
    });
}
