/*
var input = document.querySelector("#phone"),
validMsg = document.querySelector("#valid-msg"),
errorMsg = document.querySelector("#error-msg");
validMsg.innerHTML = "";
errorMsg.innerHTML = "";
var errorMap = ["Número Inválido", "Código de país inválido", "Muy Corto", "Muy Largo", "Número Inválido"];

var intl = window.intlTelInput(input, {
    allowDropdown: false,
    allowExtensions: true,
    autoFormat: false,
    autoHideDialCode: false,
    autoPlaceholder: false,
    defaultCountry: "cl",
    ipinfoToken: "yolo",
    nationalMode: false,
    numberType: "MOBILE",
    preventInvalidNumbers: true,
    initialCountry: "auto",
    geoIpLookup: function(success, failure){
        $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp){
            var countryCode = (resp && resp.country) ? resp.country:"";
            success(countryCode);
        });
    },

    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/js/utils.js"
});



var reset = function (){
    input.classList.remove("error");
    errorMsg.innerHTML = "";
    errorMsg.classList.add("hide");
    validMsg.classList.add("hide");
};

  $( "#phone" ).keyup(function() {
    input.addEventListener('blur', function(){
      reset();
      if (input.value.trim()) {
        if (intl.isValidNumber()) {
          if(input.checkValidity()) {
             //validMsg.innerHTML = "✓ Valido";
             validMsg.classList.remove("hide");
             errorMsg.classList.add("hide");
       } else {
            validMsg.innerHTML = "";
            input.classList.add("error");
            var errorCode = intl.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("error");
            validMsg.classList.add("hide");
            errorMsg.classList.remove("hide");
            input.classList.remove("error");
        }

        }else{
            validMsg.innerHTML = "";
            input.classList.add("error");
            var errorCode = intl.getValidationError();
            errorMsg.innerHTML = errorMap[errorCode];
            errorMsg.classList.remove("error");
            validMsg.classList.add("hide");
            errorMsg.classList.remove("hide");
            input.classList.remove("error");

        }
      }
    });

    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);

});*/

var Fn = {
          validaRut: function (rutCompleto) {
              if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto))
                  return false;
              var tmp = rutCompleto.split('-');
              var digv = tmp[1];
              var rut = tmp[0];
              if (digv == 'K') digv = 'k';
              return (Fn.dv(rut) == digv);
          },
          dv: function (T) {
              var M = 0, S = 1;
              for (; T; T = Math.floor(T / 10))
                  S = (S + T % 10 * (9 - M++ % 6)) % 11;
              return S ? S - 1 : 'k';
          }
      }

$(".rut").change(function(){
    if (Fn.validaRut( $(this).val() )){
        //$("#error-rut").hide();
    } else {
        //$("#error-rut").show();
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'El rut ingresado '+$(this).val()+' NO es válido, ¡Por favor corregir!',
        });
        $(this).val("");
    }
});


function validarPhone() {    
    var phoneRe = /^\+{1}56[92]\d{8}$/;
    var phone = $("#phone").val();
    var result =  phoneRe.test(phone);
    console.log('el resultado es: '+ result);
    if (!result) {
      //var errorMsg = document.querySelector("#error-msg");
      //errorMsg.innerHTML = "Numero inválido, por favor ajustar al formato solicitado, ej. +56987654321";
      $("#error-msg").show();
      console.log('false numbre');
      return false;
    }
    $("#error-msg").hide();
    return true;

}

$( "#phone" ).change(function() {
  //console.log('validar number');
  validarPhone();
});